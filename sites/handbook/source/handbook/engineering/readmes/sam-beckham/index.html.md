---
layout: markdown_page
title: "Sam Beckham's README"
---

## Sam's README

Hey my name is Sam 👋 Thanks for taking the time to read me.

I'm a Frontend Engineering Manager for the Verify stage.
I like movies, music, and long walks in Jurassic Park.

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

## Related pages

- [GitLab](https://gitlab.com/samdbeckham)
- [Twitter](https://twitter.com/samdbeckham)
- [Personal Website](https://sam.beckham.io)
- [Today's tasks](https://samdbeckham.gitlab.io/todays-tasks/)
- [5x5 Presentation](https://drive.google.com/file/d/1c6mgcQDyeEhH7Ym4Ium4p9zZsO1sbH8x/view?t=10m2s)

## About me

I started at GitLab as a Frontend Engineer on the Secure team in May 2018.
Shortly after my second anniversary, I moved over to manage the Verify frontend team and look forward to working with them more.

I'm a big film fan.
My favorite of which is Jurassic Park.
As a result of this, my ~~office~~ house is covered in dinosaur, Star Wars, and Harry Potter memorabilia.

My partner Becky and I live in [Stockton-on-Tees](https://duckduckgo.com/?q=stockton+on+tees&ia=web&iaxm=about), close to where we both grew up.
We have three cats, Luna, Pepper, and Misty; who you may see leap into action during a video call.

My drum kit doesn't get played as much as I'd like to.
But when I find the time I love putting Spotify on shuffle and jamming out to my favourite songs.
My tastes are pretty eclectic, but [00s metal](https://open.spotify.com/playlist/37i9dQZF1DWXNFSTtym834?si=KHdQM06PSCOJ6FOzOal6lQ) is my go-to.

I also used to run a meet up and conference for Frontend developers called [Frontend NE](https://frontendne.co.uk).
But have recently closed it down due to time constraints and a rather pesky global pandemic.

## My working style

Using [todoist](https://todoist.com) to keep track of everything, I use the [Getting Things Done (GTD)](https://gettingthingsdone.com/) method of working.
Where possible, I try to split tasks into smaller chunks and always look for the [boring solution](/handbook/values/#boring-solutions).
You can see what I'm working on today on [this website](https://samdbeckham.gitlab.io/todays-tasks/).

Jumping in at the deep-end is the best way for me to learn.
I may make mistakes at the beginning, but I'm not afraid to admit to them and learn from them.
I'm much more comfortable outside my comfort zone than within it.

## What I assume about others

At work, I trust people's intentions are good.
I don't expect people to have to earn my trust unless they've previously broken it.

Often, I assume people have no knowledge or context around what I'm talking about.
I feel this helps reduce questions and is useful for people watching back recorded meetings.
However, this can come across as condescending.
So please stop me if I'm explaining something that you already know.

## Communication channels

1. **GitLab:** If you have a question related to an issue or MR, please message me directly in GitLab. This is my preferred method of communication as it provides better context and can be referred to by more people, for longer.
2. **Slack:** I always have Slack running. If you have a question for me, please write it in a relevant channel and @mention me in the message. If it's a personal/private conversation then a Slack DM is fine.
3. **Zoom:** I'm always happy to jump on a zoom call. I love talking to people face-to-face and it gives me an excuse to show off my latest background.
4. **Email:** I loathe email. If you want to email, that's absolutely fine. But there's a good chance I won't see it for a long time.

## Final disclaimer

I'm still fairly early in my management journey.
I've made mistakes in the past, I know I'll make more.
Every time I make a mistake, I learn from it and get better as a result.
If you see me doing something weird, sub-optimal, or just plain wrong; please reach out to me and help me correct it.

Stay awesome.
